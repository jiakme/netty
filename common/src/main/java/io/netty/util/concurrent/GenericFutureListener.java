/*
 * Copyright 2013 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.util.concurrent;

import java.util.EventListener;

/**
 * 监听 {@link Future} 的结果.
 * <p>
 * 一旦通过调用 {@link Future#addListener(GenericFutureListener)} 来添加该监听器, 异步操作的结果将被通知.
 */
public interface GenericFutureListener<F extends Future<?>> extends EventListener {

    /**
     * 与此 {@link Future} 相关联的操作已经完成时, 该方法被调用.
     *
     * @param future 先前调用此回调的源 {@link Future}
     */
    void operationComplete(F future) throws Exception;
}
