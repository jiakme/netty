/*
 * Copyright 2016 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.util.concurrent;

/**
 * 类似于 {@link java.util.concurrent.RejectedExecutionHandler}, 但特定于 {@link SingleThreadEventExecutor}
 */
public interface RejectedExecutionHandler {

    /**
     * 当尝试添加任务到 {@link SingleThreadEventExecutor}, 但因容量限制而失败时, 被调用
     */
    void rejected(Runnable task, SingleThreadEventExecutor executor);
}
