/*
 * Copyright 2016 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.util.concurrent;

import io.netty.util.internal.ObjectUtil;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 暴露帮助方法, 用于创建不同的 {@link RejectedExecutionHandler}
 */
public final class RejectedExecutionHandlers {

    private static final RejectedExecutionHandler REJECT = new RejectedExecutionHandler() {
        @Override
        public void rejected(Runnable task, SingleThreadEventExecutor executor) {
            throw new RejectedExecutionException();
        }
    };

    private RejectedExecutionHandlers() {
    }

    /**
     * 实例将抛出 {@link RejectedExecutionException}
     */
    public static RejectedExecutionHandler reject() {
        return REJECT;
    }

    /**
     * 当因受到限制而无法添加任务时, 尝试暂缓指定的时间.
     * <p>
     * 仅当从事件循环外({@link EventExecutor#inEventLoop()} 返回 {@code false})添加该任务时, 才执行上述逻辑,
     */
    public static RejectedExecutionHandler backoff(final int retries, long backoffAmount, TimeUnit unit) {
        ObjectUtil.checkPositive(retries, "retries");
        final long backOffNanos = unit.toNanos(backoffAmount);
        return new RejectedExecutionHandler() {
            @Override
            public void rejected(Runnable task, SingleThreadEventExecutor executor) {
                if (!executor.inEventLoop()) {
                    for (int i = 0; i < retries; i++) {
                        // 尝试唤醒 executor, 这样子它的任务队列将减少
                        executor.wakeup(false);
                        // 等待 executor 执行一段时间
                        LockSupport.parkNanos(backOffNanos);
                        // 添加任务到 executor
                        if (executor.offerTask(task)) {
                            return;
                        }
                    }
                }
                // 尝试在事件循环内添加任务; 即使暂缓指定时间, 我们也无法添加该任务
                throw new RejectedExecutionException();
            }
        };
    }
}
