/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.util.concurrent;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * {@link EventExecutorGroup} 负责通过它的 {@link #next()} 方法提供要使用的 {@link EventExecutor}.
 * 除此之外, 它还负责处理它们的生命周期, 允许以全局一致地方式关闭它们.
 */
public interface EventExecutorGroup extends ScheduledExecutorService, Iterable<EventExecutor> {

    /**
     * 当且仅当该 {@link EventExecutorGroup} 管理的所有 {@link EventExecutor}
     * 正在被 {@linkplain #shutdownGracefully() 优雅关闭}, 或者
     * 已经被 {@linkplain #isShutdown() 关闭}时, 返回 {@code true}.
     */
    boolean isShuttingDown();

    /**
     * Shortcut method for {@link #shutdownGracefully(long, long, TimeUnit)} with sensible default values.
     *
     * @return the {@link #terminationFuture()}
     */
    Future<?> shutdownGracefully();

    /**
     * 通知该 executor, 调用者希望关闭它.
     * <p>
     * 一旦该方法被调用, {@link #isShuttingDown()} 开始返回 {@code true}, 该 executor 准备关闭它自身.
     * <p>
     * 不像 {@link #shutdown()}, 优雅关闭确保在关闭它自身前的 <i>'静默期(the quiet period, 通常为几秒钟)'</i> 内, 没有任务提交.
     * <p>
     * 如果在静默期内有任务提交, 那么它将确保该任务被接受, 同时将重新计算静默期.
     *
     * @param quietPeriod 静默期
     * @param timeout     直至该 executor 被 {@linkplain #shutdown()} 前, 最长等待时间, 不管是否有任务在静默期内提交
     * @param unit        {@code quietPeriod} and {@code timeout} 的单位
     * @return the {@link #terminationFuture()}
     */
    Future<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit);

    /**
     * 返回 {@link Future}, 当该 {@link EventExecutorGroup} 管理的所有 {@link EventExecutor} 均已经被中止时, 它将被通知.
     */
    Future<?> terminationFuture();

    /**
     * @deprecated {@link #shutdownGracefully(long, long, TimeUnit)} or {@link #shutdownGracefully()} instead.
     */
    @Override
    @Deprecated
    void shutdown();

    /**
     * @deprecated {@link #shutdownGracefully(long, long, TimeUnit)} or {@link #shutdownGracefully()} instead.
     */
    @Override
    @Deprecated
    List<Runnable> shutdownNow();

    /**
     * 返回该 {@link EventExecutorGroup} 管理的某个 {@link EventExecutor}
     */
    EventExecutor next();

    @Override
    Iterator<EventExecutor> iterator();

    @Override
    Future<?> submit(Runnable task);

    @Override
    <T> Future<T> submit(Runnable task, T result);

    @Override
    <T> Future<T> submit(Callable<T> task);

    @Override
    ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit);

    @Override
    <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit);

    @Override
    ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit);

    @Override
    ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit);
}
