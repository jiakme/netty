/*
 * Copyright 2013 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.util.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;


/**
 * 异步操作结果
 */
@SuppressWarnings("ClassNameSameAsAncestorName")
public interface Future<V> extends java.util.concurrent.Future<V> {

    /**
     * 当且进度该 I/O 操作完成且成功时, 返回 {@code true}
     */
    boolean isSuccess();

    /**
     * 当且仅当该操作可通过 {@link #cancel(boolean)} 取消时, 返回 {@code true}
     */
    boolean isCancellable();

    /**
     * 如果该 I/O 操作已经失败, 返回该失败 I/O 操作的原因
     *
     * @return 失败原因. 如果 succeeded 或该 future 尚未 completed, 返回 {@code null}
     */
    Throwable cause();

    /**
     * 添加指定 listener 到该 future.
     * 当该 future {@linkplain #isDone() 结束} 时, 指定 listener 将被通知.
     * 如果该 future 已经完成, 指定 listener 将立即被通知.
     */
    Future<V> addListener(GenericFutureListener<? extends Future<? super V>> listener);

    /**
     * Adds the specified listeners to this future.  The
     * specified listeners are notified when this future is
     * {@linkplain #isDone() done}.  If this future is already
     * completed, the specified listeners are notified immediately.
     */
    Future<V> addListeners(GenericFutureListener<? extends Future<? super V>>... listeners);

    /**
     * 从该 future 上移除首次出现的指定的 listener.
     * 当该 future {@linkplain #isDone() 结束} 时, 指定 listener 将不被通知.
     * 如果指定 listener 与该 future 无关, 此方法不执行任何操作, 并静默返回
     */
    Future<V> removeListener(GenericFutureListener<? extends Future<? super V>> listener);

    /**
     * Removes the first occurrence for each of the listeners from this future.
     * The specified listeners are no longer notified when this
     * future is {@linkplain #isDone() done}.  If the specified
     * listeners are not associated with this future, this method
     * does nothing and returns silently.
     */
    Future<V> removeListeners(GenericFutureListener<? extends Future<? super V>>... listeners);

    /**
     * 等待直至该 future 完成, 如果该 future 失败, 将重新抛出失败的原因.
     */
    Future<V> sync() throws InterruptedException;

    /**
     * 等待直至该 future 完成, 如果该 future 失败, 将重新抛出失败的原因.
     */
    Future<V> syncUninterruptibly();

    /**
     * 等待该 future 完成
     *
     * @throws InterruptedException 当前线程被中断
     */
    Future<V> await() throws InterruptedException;

    /**
     * Waits for this future to be completed without
     * interruption.  This method catches an {@link InterruptedException} and
     * discards it silently.
     */
    Future<V> awaitUninterruptibly();

    /**
     * Waits for this future to be completed within the
     * specified time limit.
     *
     * @return {@code true} if and only if the future was completed within
     * the specified time limit
     * @throws InterruptedException 当前线程被中断
     */
    boolean await(long timeout, TimeUnit unit) throws InterruptedException;

    /**
     * 等待该 future 在指定的时限内完成
     *
     * @return {@code true}: 当且仅当该 future 在指定时限内完成
     * @throws InterruptedException: 当前线程被中断
     */
    boolean await(long timeoutMillis) throws InterruptedException;

    /**
     * Waits for this future to be completed within the
     * specified time limit without interruption.  This method catches an
     * {@link InterruptedException} and discards it silently.
     *
     * @return {@code true} if and only if the future was completed within
     * the specified time limit
     */
    boolean awaitUninterruptibly(long timeout, TimeUnit unit);

    /**
     * Waits for this future to be completed within the
     * specified time limit without interruption.  This method catches an
     * {@link InterruptedException} and discards it silently.
     *
     * @return {@code true} if and only if the future was completed within
     * the specified time limit
     */
    boolean awaitUninterruptibly(long timeoutMillis);

    /**
     * 返回结果而不阻塞.
     * <p>
     * 如果该 future 尚未结束, 方法将返回 {@code null}.
     * <p>
     * 由于可能使用 {@code null} 值将标记 future 为成功, 因此您还需要通过 {@link #isDone()} 来区分该 future 是否真的已经完成,
     * 而不要依赖返回的 {@code null} 值.
     */
    V getNow();

    /**
     * {@inheritDoc}
     * <p>
     * If the cancellation was successful it will fail the future with a {@link CancellationException}.
     */
    @Override
    boolean cancel(boolean mayInterruptIfRunning);
}
