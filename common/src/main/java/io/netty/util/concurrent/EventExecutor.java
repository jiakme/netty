/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.util.concurrent;

/**
 * {@link EventExecutor} 是一个特殊的 {@link EventExecutorGroup}, 它带有一些便捷方法来查看一个 {@link Thread} 是否在事件循环中执行.
 * 除此之外, 它还扩展了 {@link EventExecutorGroup}, 以允许使用一种通用的形式来访问方法.
 */
public interface EventExecutor extends EventExecutorGroup {

    /**
     * 返回对自身的引用
     */
    @Override
    EventExecutor next();

    /**
     * Return the {@link EventExecutorGroup} which is the parent of this {@link EventExecutor},
     */
    EventExecutorGroup parent();

    /**
     * 使用 {@link Thread#currentThread()} 作为参数调用 {@link #inEventLoop(Thread)}
     */
    boolean inEventLoop();

    /**
     * 如果给定的 {@link Thread} 在事件循环(event loop)中执行, 则返回 {@code true}, 否则返回 {@code false}.
     */
    boolean inEventLoop(Thread thread);

    /**
     * Return a new {@link Promise}.
     */
    <V> Promise<V> newPromise();

    /**
     * Create a new {@link ProgressivePromise}.
     */
    <V> ProgressivePromise<V> newProgressivePromise();

    /**
     * 创建一个已经被标记为 succeeded 的 {@link Future}, 因此 {@link Future#isSuccess()} 将返回 {@code true}.
     * <p>
     * 所有被添加到它的 {@link FutureListener} 将直接被通知.
     * 同样, 每个对阻塞方法的调用将直接返回而不阻塞.
     */
    <V> Future<V> newSucceededFuture(V result);

    /**
     * 创建一个已经被标记为 failed 的 {@link Future}, 因此 {@link Future#isSuccess()} 将返回 {@code false}.
     * <p>
     * 所有被添加到它的 {@link FutureListener} 将直接被通知.
     * 同样, 每个对阻塞方法的调用将直接返回而不阻塞.
     */
    <V> Future<V> newFailedFuture(Throwable cause);
}
