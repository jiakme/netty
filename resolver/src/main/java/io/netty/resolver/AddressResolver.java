/*
 * Copyright 2015 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.resolver;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.Promise;
import io.netty.util.internal.UnstableApi;

import java.io.Closeable;
import java.net.SocketAddress;
import java.nio.channels.UnsupportedAddressTypeException;
import java.util.List;

/**
 * 解析一个可能的未被解析的 {@link SocketAddress}
 */
@UnstableApi
public interface AddressResolver<T extends SocketAddress> extends Closeable {

    /**
     * 当且仅当该 resolver 支持指定地址时, 返回 {@code true}
     */
    boolean isSupported(SocketAddress address);

    /**
     * 当且仅当指定地址已经被解析时, 返回 {@code true}
     *
     * @throws UnsupportedAddressTypeException: 如果此解析器不支持指定的地址
     */
    boolean isResolved(SocketAddress address);

    /**
     * 解析指定地址.
     * 如果指定地址已经被解析, 该方法不做处理并返回原始地址.
     *
     * @param address 待解析地址
     * @return 解析结果-{@link SocketAddress}
     */
    Future<T> resolve(SocketAddress address);

    /**
     * 解析指定地址.
     * 如果指定地址已经被解析, 该方法不做处理并返回原始地址.
     *
     * @param address 待解析地址
     * @param promise 当名称解析完成时, 该 {@link Promise} 将被填充
     * @return the {@link SocketAddress} as the result of the resolution
     */
    Future<T> resolve(SocketAddress address, Promise<T> promise);

    /**
     * 解析指定地址.
     * 如果指定地址已经被解析, 该方法不做处理并返回原始地址.
     *
     * @param address the address to resolve
     * @return the list of the {@link SocketAddress}es as the result of the resolution
     */
    Future<List<T>> resolveAll(SocketAddress address);

    /**
     * 解析指定地址.
     * 如果指定地址已经被解析, 该方法不做处理并返回原始地址.
     *
     * @param address the address to resolve
     * @param promise the {@link Promise} which will be fulfilled when the name resolution is finished
     * @return the list of the {@link SocketAddress}es as the result of the resolution
     */
    Future<List<T>> resolveAll(SocketAddress address, Promise<List<T>> promise);

    /**
     * Closes all the resources allocated and used by this resolver.
     */
    @Override
    void close();
}
