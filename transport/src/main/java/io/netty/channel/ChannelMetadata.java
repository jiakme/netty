/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.channel;

import java.net.SocketAddress;

import static io.netty.util.internal.ObjectUtil.checkPositive;

/**
 * 代表一种 {@link Channel} 实现的属性.
 */
public final class ChannelMetadata {

    private final boolean hasDisconnect;
    private final int defaultMaxMessagesPerRead;

    /**
     * Create a new instance
     *
     * @param hasDisconnect {@code true}: 当且仅当该 channel 拥有允许用户断开然后再次调用
     *                      {@link Channel#connect(SocketAddress)} 的 {@code disconnect()} 操作, 例如 UDP/IP.
     */
    public ChannelMetadata(boolean hasDisconnect) {
        this(hasDisconnect, 1);
    }

    /**
     * Create a new instance
     *
     * @param hasDisconnect             {@code true}: 当且仅当该 channel 拥有允许用户断连然后再次调用
     *                                  {@link Channel#connect(SocketAddress)} 的 {@code disconnect()} 操作, 如  UDP/IP.
     * @param defaultMaxMessagesPerRead 如果在使用 {@link MaxMessagesRecvByteBufAllocator}, 则将为
     *                                  {@link MaxMessagesRecvByteBufAllocator#maxMessagesPerRead()} 设置此值.
     *                                  必须大于 {@code > 0}.
     */
    public ChannelMetadata(boolean hasDisconnect, int defaultMaxMessagesPerRead) {
        checkPositive(defaultMaxMessagesPerRead, "defaultMaxMessagesPerRead");
        this.hasDisconnect = hasDisconnect;
        this.defaultMaxMessagesPerRead = defaultMaxMessagesPerRead;
    }

    /**
     * 当且仅当该 channel 拥有允许用户断开连接然后再次调用 {@link Channel#connect(SocketAddress)} 的 {@code disconnect()} 操作,
     * 如 UDP/IP, 才返回 {@code true}.
     */
    public boolean hasDisconnect() {
        return hasDisconnect;
    }

    /**
     * 如果在使用 {@link MaxMessagesRecvByteBufAllocator}, 那么它将是
     * {@link MaxMessagesRecvByteBufAllocator#maxMessagesPerRead()} 的默认值.
     */
    public int defaultMaxMessagesPerRead() {
        return defaultMaxMessagesPerRead;
    }
}
