/*
 * Copyright 2013 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

package io.netty.channel;

import java.io.Serializable;

/**
 * 表示 {@link Channel} 的全局唯一标识符.
 * <p>
 * 标识符是从以下列出的各种来源生成的:
 * <ul>
 * <li>MAC 地址 (EUI-48 or EUI-64) 或网络适配器(network adapter), 最好是全局唯一的适配器</li>
 * <li>当前进程 ID</li>
 * <li>{@link System#currentTimeMillis()},</li>
 * <li>{@link System#nanoTime()},</li>
 * <li>随机 32-bit 整数</li>
 * <li>顺序递增的 32-bit 整数</li>
 * </ul>
 * <p>
 * 生成的标识符的全局唯一性主要取决于 MAC 地址和当前进程 ID, 在类加载时尽最大可能检测它们.
 * 如果对它们获取的尝试均失败, 则会记录一条警告消息, 并使用随机值代替.
 * 或者, 您也可以通过系统属性手动指定它们:
 * <ul>
 * <li>{@code io.netty.machineId} - 48 (或 64)位整数的十六进制表示形式, 可以选择用冒号或连字符(-)分隔</li>
 * <li>{@code io.netty.processId} - 0 and 65535 之前的整数</li>
 * </ul>
 */
public interface ChannelId extends Serializable, Comparable<ChannelId> {

    /**
     * 返回该 {@link ChannelId} 的简短但非全局唯一的字符表示
     */
    String asShortText();

    /**
     * 返回该 {@link ChannelId} 的长的但全局唯一的字符表示
     */
    String asLongText();
}
