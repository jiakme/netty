/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.channel;

/**
 * 添加状态变更回调的 {@link ChannelHandler}.
 * 这允许用户可以轻松地勾住(hook)状态变更.
 */
public interface ChannelInboundHandler extends ChannelHandler {

    /**
     * 该 {@link ChannelHandlerContext} 的 {@link Channel} 被注册到它的 {@link EventLoop}
     */
    void channelRegistered(ChannelHandlerContext ctx) throws Exception;

    /**
     * 该 {@link ChannelHandlerContext} 的 {@link Channel} 已从它的 {@link EventLoop} 中取消注册
     */
    void channelUnregistered(ChannelHandlerContext ctx) throws Exception;

    /**
     * 该 {@link ChannelHandlerContext} 的 {@link Channel} 现在被激活
     */
    void channelActive(ChannelHandlerContext ctx) throws Exception;

    /**
     * 该 {@link ChannelHandlerContext} 的已被注册的 {@link Channel} 现在已经处于非激活状态, 并达到了它生命周期的终点
     */
    void channelInactive(ChannelHandlerContext ctx) throws Exception;

    /**
     * 当前 {@link Channel} 已经从连节点读取了一条 message
     */
    void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception;

    /**
     * 当前读取操作读取的最后一条 message 被 {@link #channelRead(ChannelHandlerContext, Object)} 消耗时, 该方法被调用.
     * 如果 {@link ChannelOption＃AUTO_READ} 关闭, 那么在调用 {@link ChannelHandlerContext#read()} 之前,
     * 将不再尝试从当前 {@link Channel} 读取 inbound 数据
     */
    void channelReadComplete(ChannelHandlerContext ctx) throws Exception;

    /**
     * 如果一个用户事件被触发, 该方法被调用
     */
    void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception;

    /**
     * 一旦一个 {@link Channel} 的 writable 状态变更, 该方法被调用.
     * 您可以通过使用 {@link Channel#isWritable()} 来检查该状态
     */
    void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception;

    /**
     * 如果一个 {@link Throwable} 被抛出, 该方法被调用
     */
    @Override
    @SuppressWarnings("deprecation")
    void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception;
}
