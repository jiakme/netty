/*
 * Copyright 2013 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.channel;

/**
 * 负责估计 message 的大小.
 * 该大小大约代表 message 将在内存中保留多少内存.
 */
public interface MessageSizeEstimator {

    /**
     * 创建一个新的 handle.
     * 该 handle 提供实际的操作.
     */
    Handle newHandle();

    interface Handle {

        /**
         * 计算给定 message 的大小
         *
         * @param msg 待计算大小的消息
         * @return byte 数. 返回大小必须 >= 0
         */
        int size(Object msg);
    }
}
