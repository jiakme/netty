/*
 * Copyright 2015 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.channel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.UncheckedBooleanSupplier;

import static io.netty.util.internal.ObjectUtil.checkPositive;

/**
 * {@link MaxMessagesRecvByteBufAllocator} 默认实现, 它尊重 {@link ChannelConfig#isAutoRead()}, 并防止溢出
 */
public abstract class DefaultMaxMessagesRecvByteBufAllocator implements MaxMessagesRecvByteBufAllocator {

    /**
     * 每次 read 读取 message 的最大次数
     */
    private volatile int maxMessagesPerRead;
    private volatile boolean respectMaybeMoreData = true;

    /**
     * 默认构造器, maxMessagesPerRead = 1
     */
    public DefaultMaxMessagesRecvByteBufAllocator() {
        this(1);
    }

    /**
     * @param maxMessagesPerRead 每次 read 读取 message 的最大次数
     */
    public DefaultMaxMessagesRecvByteBufAllocator(int maxMessagesPerRead) {
        maxMessagesPerRead(maxMessagesPerRead);
    }

    @Override
    public int maxMessagesPerRead() {
        return maxMessagesPerRead;
    }

    @Override
    public MaxMessagesRecvByteBufAllocator maxMessagesPerRead(int maxMessagesPerRead) {
        checkPositive(maxMessagesPerRead, "maxMessagesPerRead");
        this.maxMessagesPerRead = maxMessagesPerRead;
        return this;
    }

    /**
     * 用以确定, 如果我们认为没有更多数据, 后面 {@link #newHandle()} 实例是否将停止读取
     *
     * @param respectMaybeMoreData <ul>
     *                             <li>{@code true}: 如果我们认为没有更多数据, 停止读取.
     *                             这样可以省去一次用以从 socket 读取数据的系统调用,
     *                             但是, 如果数据以简洁的方式(racy fashion)到达, 我们可能会放弃 {@link #maxMessagesPerRead()} 额度,
     *                             必须等待 selector 在有更多数据时通知我们.</li>
     *                             <li>{@code false}: 保持读取(直至 {@link #maxMessagesPerRead()}),
     *                             或者直至当我们尝试读取读取时没有更多数据</li>
     *                             </ul>
     */
    public DefaultMaxMessagesRecvByteBufAllocator respectMaybeMoreData(boolean respectMaybeMoreData) {
        this.respectMaybeMoreData = respectMaybeMoreData;
        return this;
    }

    /**
     * Get if future instances of {@link #newHandle()} will stop reading if we think there is no more data.
     *
     * @return <ul>
     * <li>{@code true} to stop reading if we think there is no more data. This may save a system call to read from
     * the socket, but if data has arrived in a racy fashion we may give up our {@link #maxMessagesPerRead()}
     * quantum and have to wait for the selector to notify us of more data.</li>
     * <li>{@code false} to keep reading (up to {@link #maxMessagesPerRead()}) or until there is no data when we
     * attempt to read.</li>
     * </ul>
     */
    public final boolean respectMaybeMoreData() {
        return respectMaybeMoreData;
    }

    /**
     * Focuses on enforcing the maximum messages per read condition for {@link #continueReading()}.
     */
    public abstract class MaxMessageHandle implements ExtendedHandle {
        private final boolean respectMaybeMoreData = DefaultMaxMessagesRecvByteBufAllocator.this.respectMaybeMoreData;
        private ChannelConfig config;
        // 每次 read 读取 message 的最大次数
        private int maxMessagePerRead;
        // 已读取 message 总数
        private int totalMessages;
        // 已读取 byte 总数
        private int totalBytesRead;
        // 尝试取 byte 总数
        private int attemptedBytesRead;
        // 上一次 read 读取的 byte 数
        private int lastBytesRead;
        private final UncheckedBooleanSupplier defaultMaybeMoreSupplier = new UncheckedBooleanSupplier() {
            @Override
            public boolean get() {
                return attemptedBytesRead == lastBytesRead;
            }
        };

        /**
         * Only {@link ChannelConfig#getMaxMessagesPerRead()} is used.
         */
        @Override
        public void reset(ChannelConfig config) {
            this.config = config;
            maxMessagePerRead = maxMessagesPerRead();
            totalMessages = totalBytesRead = 0;
        }

        @Override
        public ByteBuf allocate(ByteBufAllocator alloc) {
            return alloc.ioBuffer(guess());
        }

        @Override
        public final void incMessagesRead(int amt) {
            totalMessages += amt;
        }

        @Override
        public void lastBytesRead(int bytes) {
            lastBytesRead = bytes;
            if (bytes > 0) {
                totalBytesRead += bytes;
            }
        }

        @Override
        public final int lastBytesRead() {
            return lastBytesRead;
        }

        @Override
        public boolean continueReading() {
            return continueReading(defaultMaybeMoreSupplier);
        }

        @Override
        public boolean continueReading(UncheckedBooleanSupplier maybeMoreDataSupplier) {
            return config.isAutoRead() &&
                    (!respectMaybeMoreData || maybeMoreDataSupplier.get()) &&
                    // 限制读取次数
                    totalMessages < maxMessagePerRead &&
                    totalBytesRead > 0;
        }

        @Override
        public void readComplete() {
        }

        @Override
        public int attemptedBytesRead() {
            return attemptedBytesRead;
        }

        @Override
        public void attemptedBytesRead(int bytes) {
            attemptedBytesRead = bytes;
        }

        protected final int totalBytesRead() {
            return totalBytesRead < 0 ? Integer.MAX_VALUE : totalBytesRead;
        }
    }
}
