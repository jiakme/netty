/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.channel;

import io.netty.util.concurrent.EventExecutorGroup;

/**
 * 特殊的 {@link EventExecutorGroup}, 它允许注册 {@link Channel},
 * 这些 {@link Channel} 将在 event loop 后面的 select 步骤中得到处理.
 */
public interface EventLoopGroup extends EventExecutorGroup {

    /**
     * 返回下一个将使用的 {@link EventLoop}
     */
    @Override
    EventLoop next();

    /**
     * 使用此 {@link EventLoop} 注册一个 {@link Channel}.
     * <p>
     * 注册完成后, 将通知返回的 {@link ChannelFuture}.
     */
    ChannelFuture register(Channel channel);

    /**
     * 使用一个 {@link ChannelFuture} 在此 {@link EventLoop} 中注册一个 {@link Channel}.
     * 一旦注册完成, 传入的 {@link ChannelFuture} 将收到通知, 并且得到返回.
     */
    ChannelFuture register(ChannelPromise promise);

    /**
     * Register a {@link Channel} with this {@link EventLoop}. The passed {@link ChannelFuture}
     * will get notified once the registration was complete and also will get returned.
     *
     * @deprecated Use {@link #register(ChannelPromise)} instead.
     */
    @Deprecated
    ChannelFuture register(Channel channel, ChannelPromise promise);
}
