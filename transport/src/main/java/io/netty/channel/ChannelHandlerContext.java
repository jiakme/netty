/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.channel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import io.netty.util.AttributeMap;
import io.netty.util.concurrent.EventExecutor;

import java.nio.channels.Channels;

/**
 * 使得 {@link ChannelHandler} 能够同它的 {@link ChannelPipeline} 和其它 handlers 互动.
 * 除其它事项外, 一个 handler 可以通知 {@link ChannelPipeline} 中的下一 {@link ChannelHandler},
 * 同样可以动态修改其所属的 {@link ChannelPipeline}.
 *
 * <h3>通知</h3>
 * <p>
 * You can notify the closest handler in the same {@link ChannelPipeline} by calling one of the various methods
 * provided here.
 * <p>
 * 请参阅 {@link ChannelPipeline} 以了解 event 如何流转.
 *
 * <h3>修改 pipeline</h3>
 * <p>
 * 您可以通过调用 {@link #pipeline()} 来获取该 handler 所属的 {@link ChannelPipeline}.
 * 应用可以在运行时动态地 insert, remove, or replace handlers.
 *
 * <h3>提取以备后用</h3>
 * <p>
 * 你可以持有 {@link ChannelHandlerContext} 以备后用, 例如在 handler 方法外触发一个事件, 甚至是从不同的线程.
 * <pre>
 * public class MyHandler extends {@link ChannelDuplexHandler} {
 *
 *     <b>private {@link ChannelHandlerContext} ctx;</b>
 *
 *     public void beforeAdd({@link ChannelHandlerContext} ctx) {
 *         <b>this.ctx = ctx;</b>
 *     }
 *
 *     public void login(String username, password) {
 *         ctx.write(new LoginMessage(username, password));
 *     }
 *     ...
 * }
 * </pre>
 *
 * <h3>存储状态信息</h3>
 * <p>
 * {@link #attr(AttributeKey)} 允许您存储和访问某个 handler 和它 context 相关的状态信息.
 * 请参阅 {@link ChannelHandler} 以学习多个推荐的用于管理状态信息的方式.
 *
 * <h3>一个 handler 可以拥有多个 context</h3>
 * <p>
 * Please note that a {@link ChannelHandler} instance can be added to more than
 * one {@link ChannelPipeline}.  It means a single {@link ChannelHandler}
 * instance can have more than one {@link ChannelHandlerContext} and therefore
 * the single instance can be invoked with different
 * {@link ChannelHandlerContext}s if it is added to one or more
 * {@link ChannelPipeline}s more than once.
 * <p>
 * For example, the following handler will have as many independent {@link AttributeKey}s
 * as how many times it is added to pipelines, regardless if it is added to the
 * same pipeline multiple times or added to different pipelines multiple times:
 * <pre>
 * public class FactorialHandler extends {@link ChannelInboundHandlerAdapter} {
 *
 *   private final {@link AttributeKey}&lt;{@link Integer}&gt; counter = {@link AttributeKey}.valueOf("counter");
 *
 *   // This handler will receive a sequence of increasing integers starting
 *   // from 1.
 *   {@code @Override}
 *   public void channelRead({@link ChannelHandlerContext} ctx, Object msg) {
 *     Integer a = ctx.attr(counter).get();
 *
 *     if (a == null) {
 *       a = 1;
 *     }
 *
 *     attr.set(a * (Integer) msg);
 *   }
 * }
 *
 * // Different context objects are given to "f1", "f2", "f3", and "f4" even if
 * // they refer to the same handler instance.  Because the FactorialHandler
 * // stores its state in a context object (using an {@link AttributeKey}), the factorial is
 * // calculated correctly 4 times once the two pipelines (p1 and p2) are active.
 * FactorialHandler fh = new FactorialHandler();
 *
 * {@link ChannelPipeline} p1 = {@link Channels}.pipeline();
 * p1.addLast("f1", fh);
 * p1.addLast("f2", fh);
 *
 * {@link ChannelPipeline} p2 = {@link Channels}.pipeline();
 * p2.addLast("f3", fh);
 * p2.addLast("f4", fh);
 * </pre>
 *
 * <h3>值得额外查阅的资源</h3>
 * <p>
 * 请参阅 {@link ChannelHandler} 和 {@link ChannelPipeline}, 以了解更多关于 inbound and outbound 操作, 它们之间的本质区别,
 * 它们如何在 pipeline 中流动, 以及如何在应用中处理这些操作(指出入站操作?)等
 */
public interface ChannelHandlerContext extends AttributeMap, ChannelInboundInvoker, ChannelOutboundInvoker {

    /**
     * 被绑定至该 {@link ChannelHandlerContext} 的 {@link Channel}
     */
    Channel channel();

    /**
     * 返回用于执行任意任务的 {@link EventExecutor}.
     */
    EventExecutor executor();

    /**
     * The unique name of the {@link ChannelHandlerContext}.The name was used when then {@link ChannelHandler}
     * was added to the {@link ChannelPipeline}. This name can also be used to access the registered
     * {@link ChannelHandler} from the {@link ChannelPipeline}.
     */
    String name();

    /**
     * 与此 {@link ChannelHandlerContext} 绑定的 {@link ChannelHandler}
     */
    ChannelHandler handler();

    /**
     * 如果该 {@link ChannelHandler} 已经从所属 context 的 {@link ChannelPipeline} 中移除, 返回 {@code true}.
     * 注意: 此方法近用于从 {@link EventLoop} 中调用.
     */
    boolean isRemoved();

    @Override
    ChannelHandlerContext fireChannelRegistered();

    @Override
    ChannelHandlerContext fireChannelUnregistered();

    @Override
    ChannelHandlerContext fireChannelActive();

    @Override
    ChannelHandlerContext fireChannelInactive();

    @Override
    ChannelHandlerContext fireExceptionCaught(Throwable cause);

    @Override
    ChannelHandlerContext fireUserEventTriggered(Object evt);

    @Override
    ChannelHandlerContext fireChannelRead(Object msg);

    @Override
    ChannelHandlerContext fireChannelReadComplete();

    @Override
    ChannelHandlerContext fireChannelWritabilityChanged();

    @Override
    ChannelHandlerContext read();

    @Override
    ChannelHandlerContext flush();

    /**
     * 返回分配的 {@link ChannelPipeline}
     */
    ChannelPipeline pipeline();

    /**
     * 返回分配的 {@link ByteBufAllocator}, 它将被用于分配 {@link ByteBuf
     */
    ByteBufAllocator alloc();

    /**
     * @deprecated Use {@link Channel#attr(AttributeKey)}
     */
    @Deprecated
    @Override
    <T> Attribute<T> attr(AttributeKey<T> key);

    /**
     * @deprecated Use {@link Channel#hasAttr(AttributeKey)}
     */
    @Deprecated
    @Override
    <T> boolean hasAttr(AttributeKey<T> key);
}
