/*
 * Copyright 2015 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.buffer;

/**
 * Metrics for a chunk.
 */
public interface PoolChunkMetric {

    /**
     * 返回该 chunk 当前使用量的百分比
     */
    int usage();

    /**
     * 返回该 chunk 的大小(byte), 这是该 chunk 可包含的最大字节数
     */
    int chunkSize();

    /**
     * 该 chunk 中的空闲 bytes 数
     */
    int freeBytes();
}
