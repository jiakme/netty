/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.buffer;

/**
 * 实现负责分配 buffer.
 *
 * 该接口的实现应该是线程安全的
 */
public interface ByteBufAllocator {

    ByteBufAllocator DEFAULT = ByteBufUtil.DEFAULT_ALLOCATOR;

    /**
     * 分配 {@link ByteBuf}.
     *
     * 是 direct 还是 heap buffer 取决于具体实现.
     */
    ByteBuf buffer();

    /**
     * 使用给定初始容量分配 {@link ByteBuf}
     *
     * 是 direct 还是 heap buffer 取决于具体实现.
     */
    ByteBuf buffer(int initialCapacity);

    /**
     * 使用给定初始容量和最大容量分配 {@link ByteBuf}
     *
     * 是 direct 还是 heap buffer 取决于具体实现.
     */
    ByteBuf buffer(int initialCapacity, int maxCapacity);

    /**
     * 分配 {@link ByteBuf}, 倾向于 direct buffer, 因为它适用于 I/O.
     */
    ByteBuf ioBuffer();

    /**
     * 分配 {@link ByteBuf}, 倾向于 direct buffer, 因为它适用于 I/O.
     */
    ByteBuf ioBuffer(int initialCapacity);

    /**
     * 分配 {@link ByteBuf}, 倾向于 direct buffer, 因为它适用于 I/O.
     */
    ByteBuf ioBuffer(int initialCapacity, int maxCapacity);

    /**
     * 分配 heap {@link ByteBuf}.
     */
    ByteBuf heapBuffer();

    /**
     * 使用给定初始容量分配 heap {@link ByteBuf}
     */
    ByteBuf heapBuffer(int initialCapacity);

    /**
     * 使用给定初始容量和最大容量分配 heap {@link ByteBuf}
     */
    ByteBuf heapBuffer(int initialCapacity, int maxCapacity);

    /**
     * 分配 direct {@link ByteBuf}.
     */
    ByteBuf directBuffer();

    /**
     * 使用给定初始容量分配 direct {@link ByteBuf}
     */
    ByteBuf directBuffer(int initialCapacity);

    /**
     * 使用给定初始容量和最大容量分配 direct {@link ByteBuf}
     */
    ByteBuf directBuffer(int initialCapacity, int maxCapacity);

    /**
     * 分配 {@link CompositeByteBuf}.
     *
     * 是 direct 还是 heap buffer 取决于具体实现.
     */
    CompositeByteBuf compositeBuffer();

    /**
     * 分配 {@link CompositeByteBuf}, 并指定可被存储的最大组件数.
     *
     * 是 direct 还是 heap buffer 取决于具体实现.
     */
    CompositeByteBuf compositeBuffer(int maxNumComponents);

    /**
     * 分配 heap {@link CompositeByteBuf}.
     */
    CompositeByteBuf compositeHeapBuffer();

    /**
     * 分配 heap {@link CompositeByteBuf}, 并指定可被存储的最大组件数.
     */
    CompositeByteBuf compositeHeapBuffer(int maxNumComponents);

    /**
     * 分配 direct {@link CompositeByteBuf}.
     */
    CompositeByteBuf compositeDirectBuffer();

    /**
     * 分配 direct {@link CompositeByteBuf}, 并指定可被存储的最大组件数.
     */
    CompositeByteBuf compositeDirectBuffer(int maxNumComponents);

    /**
     * 如果 direct {@link ByteBuf} 被池化, 返回 {@code true}
     */
    boolean isDirectBufferPooled();

    /**
     * 计算 {@link ByteBuf} 扩张需要的新容量
     *
     * @param minNewCapacity 最小新容量
     * @param maxCapacity 容量上边界
     */
    int calculateNewCapacity(int minNewCapacity, int maxCapacity);
 }
