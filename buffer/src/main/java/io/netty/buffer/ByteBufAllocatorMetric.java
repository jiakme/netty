/*
 * Copyright 2017 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package io.netty.buffer;

/**
 * ByteBufAllocator 度量
 */
public interface ByteBufAllocatorMetric {

    /**
     * 返回 {@link ByteBufAllocator} 使用的 heap 内存的字节数量, 如果未知, 返回 {@code -1}
     */
    long usedHeapMemory();

    /**
     * 返回 {@link ByteBufAllocator} 使用的 direct 内存的字节数量, 如果未知, 返回 {@code -1}
     */
    long usedDirectMemory();
}
